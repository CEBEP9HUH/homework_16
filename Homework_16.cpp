﻿#include <iostream>
#include <time.h>

int main()
{
    const int N = 5;
    int arr[N][N];
    int sum = 0;
    for (auto i = 0; i < N; ++i)
    {
        for (auto j = 0; j < N; ++j)
        {
            arr[i][j] = i + j;
            std::cout << arr[i][j] << "\t";
        }
        std::cout << "\n";
    }
    time_t t = time(nullptr);
    tm tmpTimeStruct;
    localtime_s(&tmpTimeStruct, &t);
    int month = tmpTimeStruct.tm_mday;
    int lineNumber = month % N - 1;
    std::cout << "Line for current day of month is: " << std::endl;
    for (auto j = 0; j < N; ++j)
    {
        std::cout << arr[lineNumber][j] << "\t";
        sum += arr[lineNumber][j];
    }
    std::cout << "\nSum of elements in this line is: " << sum << std::endl;

}